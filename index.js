db.Rooms.insertOne({
	"name": "single",
	"accomodations": "2",
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms": "10",
	"isAvailable": false
})

db.Rooms.insertMany([
{

	"name": "Double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small going on a vacation",
	"room_available": 5,
	"isAvailable": false
},
])

db.Rooms.insertMany([
{

	"name": "queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"room_available": 15,
	"isAvailable": false
},
])

//5. Use the find method to search for a room with the name double.

db.Rooms.find({"name": "Double"})

//6. Use the updateOne method to update the queen room and set the available rooms to 0.

db.Rooms.updateOne(
	{"name": "queen"},
		{
			$set: {
				"room_available": 0,
			}
		}
)

//7. Use the deleteMany method rooms to delete all rooms that have 0 availability.

db.Rooms.deleteMany({
	"room_available": 0,
})

